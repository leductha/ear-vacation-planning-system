package cvut.fel.vacation.endpoints;

import cvut.fel.vacation.dto.EmployeeGetDTO;
import cvut.fel.vacation.dto.VacationRequestGetDTO;
import cvut.fel.vacation.dto.VacationRequestPostDTO;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.State;
import cvut.fel.vacation.model.VacationRequest;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;


import java.time.LocalDate;

public class VacationRequestEndpointsTest extends BaseCT {
    @Test
    void postRequestWillBeSuccessfulTest() {
        VacationRequestPostDTO request = createVacationRequestDTO();

        HttpHeaders header = new HttpHeaders();
        header.setBearerAuth(authToken);
        HttpEntity<?> requestEntity = new HttpEntity<>(request, header);

        ResponseEntity<VacationRequestGetDTO> response = testRestTemplate.exchange(
                "/request",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<>() {}
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        VacationRequest vacationRequest = vacationRequestService.find(1);
        if (vacationRequest != null) {
            VacationRequestGetDTO expectedRequest = VacationRequestGetDTO.fromEntity(vacationRequest);
            assertThat(response.getBody()).usingRecursiveComparison().isEqualTo(expectedRequest);
        }
    }

    @Test
    void removeVacationRequestThenReturnsEmptyArrayTest() {
        createVacationRequest();
        assertThat(vacationRequestService.findAll().size()).isEqualTo(1);

        HttpEntity<?> requestEntity = tokenHeader();
        ResponseEntity<?> response = testRestTemplate.exchange(
                "/request/1",
                HttpMethod.DELETE,
                requestEntity,
                new ParameterizedTypeReference<>() {}
        );

        assertThat(vacationRequestService.findAll()).isEmpty();
    }

    @Test
    void assignRequestToAdminTest() {
        final int id = 1;
        final String link = UriComponentsBuilder.fromPath("/request/{id}/assign")
                .buildAndExpand(id)
                .toString();

        createVacationRequest();

        HttpEntity<?> requestEntity = tokenHeader();
        ResponseEntity<VacationRequestGetDTO> response = testRestTemplate.exchange(
                link,
                HttpMethod.PUT,
                requestEntity,
                VacationRequestGetDTO.class
        );

        VacationRequest request = vacationRequestService.find(id);
        if(request != null) {
            VacationRequestGetDTO expectedRequest = VacationRequestGetDTO.fromEntity(request);
            assertThat(response.getBody()).usingRecursiveComparison().isEqualTo(expectedRequest);
        }
    }

    @Test
    void approveRequestTest() {
        final int id = 1;
        final String link = UriComponentsBuilder.fromPath("/request/{id}/approve")
                .buildAndExpand(id)
                .toString();

        createAssignedRequest();

        HttpEntity<?> requestEntity = tokenHeader();
        ResponseEntity<VacationRequestGetDTO> response = testRestTemplate.exchange(
                link,
                HttpMethod.PUT,
                requestEntity,
                VacationRequestGetDTO.class
        );

        VacationRequest request = vacationRequestService.find(id);

        if(request != null) {
            assertThat(request.getState()).isEqualTo(State.APPROVED);

            VacationRequestGetDTO expectedRequest = VacationRequestGetDTO.fromEntity(request);
            assertThat(response.getBody()).usingRecursiveComparison().isEqualTo(expectedRequest);
        }
    }

    @Test
    void denyRequestTest() {
        final int id = 1;
        final String link = UriComponentsBuilder.fromPath("/request/{id}/deny")
                .buildAndExpand(id)
                .toString();

        createAssignedRequest();

        HttpEntity<?> requestEntity = tokenHeader();
        ResponseEntity<VacationRequestGetDTO> response = testRestTemplate.exchange(
                link,
                HttpMethod.PUT,
                requestEntity,
                VacationRequestGetDTO.class
        );

        VacationRequest request = vacationRequestService.find(id);
        if(request != null) {
            assertThat(request.getState()).isEqualTo(State.DENIED);

            VacationRequestGetDTO expectedRequest = VacationRequestGetDTO.fromEntity(request);
            assertThat(response.getBody()).usingRecursiveComparison().isEqualTo(expectedRequest);
        }
    }

    private VacationRequest createVacationRequest() {
        VacationRequest request = new VacationRequest();
        request.setFromDate(LocalDate.now());
        request.setToDate(LocalDate.now().plusDays(20));
        request.setEmployee(admin);

        vacationRequestService.save(request);
        return request;
    }

    private VacationRequest createAssignedRequest() {
        VacationRequest request = new VacationRequest();
        request.setFromDate(LocalDate.now());
        request.setToDate(LocalDate.now().plusDays(10));
        request.setEmployee(createUser("Pablo", "Picasso", "peepee", "123"));
        request.setAdmin(admin);

        vacationRequestService.save(request);
        return request;
    }

    private VacationRequestPostDTO createVacationRequestDTO() {
        VacationRequestPostDTO dto = new VacationRequestPostDTO();
        dto.setFromDate(LocalDate.now());
        dto.setToDate(LocalDate.now().plusDays(10));

        return dto;
    }


}
