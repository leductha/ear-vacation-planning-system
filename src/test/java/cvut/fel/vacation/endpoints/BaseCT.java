package cvut.fel.vacation.endpoints;

import cvut.fel.vacation.endpoints.config.TestRestTemplateConfig;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Role;
import cvut.fel.vacation.rest.auth.JwtResponse;
import cvut.fel.vacation.rest.auth.LoginRequest;
import cvut.fel.vacation.service.EmployeeService;
import cvut.fel.vacation.service.VacationRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest(classes = TestRestTemplateConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class BaseCT {

    @Autowired
    protected TestRestTemplate testRestTemplate;

    @Autowired
    protected EmployeeService employeeService;

    @Autowired
    protected VacationRequestService vacationRequestService;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    protected String authToken;

    protected Employee admin;

    @BeforeEach
    void getAuthToken() {
        employeeService.removeAll();

        createAdmin();

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("admin");
        loginRequest.setPassword("admin");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LoginRequest> requestEntity = new HttpEntity<>(loginRequest, headers);

        ResponseEntity<JwtResponse> response = testRestTemplate.postForEntity("/auth/login", requestEntity, JwtResponse.class);
        authToken = response.getBody().getToken();
    }

    public void createAdmin() {
        Employee employee = new Employee();
        employee.setPassword(passwordEncoder.encode("admin"));
        employee.setUsername("admin");
        employee.setLastName("admin");
        employee.setFirstName("admin");
        employee.setRole(Role.ROLE_ADMIN);
        employeeService.save(employee);
        admin = employee;
    }

    public HttpEntity<?> tokenHeader() {
        HttpHeaders header = new HttpHeaders();
        header.setBearerAuth(authToken);
        HttpEntity<?> requestEntity = new HttpEntity<>(header);

        return requestEntity;
    }

    public Employee createUser(String firstname, String lastname, String username, String password) {
        Employee employee = new Employee();

        employee.setFirstName(firstname);
        employee.setLastName(lastname);
        employee.setUsername(username);
        employee.setPassword(passwordEncoder.encode(password));
        employee.setRole(Role.ROLE_USER);

        employeeService.save(employee);
        return employee;
    }

}