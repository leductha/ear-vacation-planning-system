package cvut.fel.vacation.endpoints;

import cvut.fel.vacation.dto.EmployeeGetDTO;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Role;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.junit.jupiter.api.Test;


import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class EmployeeEndpointsTest extends BaseCT {

    @Test
    void getAllEmployeesWillReturnOnlyAdminTest() {
        EmployeeGetDTO adminDTO = EmployeeGetDTO.fromEntity(admin);

        HttpEntity<?> requestEntity = tokenHeader();

        ResponseEntity<List<EmployeeGetDTO>> response = testRestTemplate.exchange(
                "/employee",
                HttpMethod.GET,
                requestEntity,
                new ParameterizedTypeReference<>() {}
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(adminDTO).usingRecursiveComparison().isEqualTo(response.getBody().get(0));
    }

    @Test
    void retrieveAllSavedEmployeesTest() {
        List<Employee> list = List.of (
                admin,
                createUser("Justin", "Bieber", "jb", "123"),
                createUser("312321", "3132", "312123", "312213123"),
                createUser("31232132", "312312", "4321454", "7645756")
        );

        List<EmployeeGetDTO> expected = list.stream()
                .map(EmployeeGetDTO::fromEntity)
                .collect(Collectors.toList());

        HttpEntity<?> requestEntity = tokenHeader();

        ResponseEntity<List<EmployeeGetDTO>> response = testRestTemplate.exchange(
                "/employee",
                HttpMethod.GET,
                requestEntity,
                new ParameterizedTypeReference<>() {}
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void afterEmployeeRemovalCannotBeFoundTest() {
        Employee employee = createUser("James", "Bond", "007", "123");
        List<Employee> employees = employeeService.findAll();

        assertThat(employees).isNotEmpty();
        assertThat(employees.stream().anyMatch(e -> e.getId().equals(employee.getId()))).isTrue();

        HttpEntity<?> requestEntity = tokenHeader();
        testRestTemplate.exchange(
                "/employee/2",
                HttpMethod.DELETE,
                requestEntity,
                new ParameterizedTypeReference<>() {}
        );

        List<Employee> employeesAfterDeletion = employeeService.findAll();
        assertThat(employeesAfterDeletion.size()).isEqualTo(1);
        assertThat(employeesAfterDeletion.contains(employee)).isFalse();
    }




}
