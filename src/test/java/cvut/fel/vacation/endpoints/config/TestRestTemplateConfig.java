package cvut.fel.vacation.endpoints.config;
import lombok.AllArgsConstructor;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

@TestConfiguration
@AllArgsConstructor
public class TestRestTemplateConfig {
    private final RestTemplateBuilder restTemplateBuilder;
    private final ServletWebServerApplicationContext servletWebServer;

    @Bean
    public TestRestTemplate testRestTemplate() {

        return new TestRestTemplate(restTemplateBuilder
                .rootUri("http://localhost:" + servletWebServer.getWebServer().getPort()));
    }

    @Bean
    public TestRestTemplate testRestTemplateBasicAuth() {
        return new TestRestTemplate(restTemplateBuilder
                .rootUri("http://localhost:" + servletWebServer.getWebServer().getPort()));
    }
}
