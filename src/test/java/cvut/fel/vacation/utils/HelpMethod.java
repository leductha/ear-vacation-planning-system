package cvut.fel.vacation.utils;

import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Role;
import cvut.fel.vacation.model.Team;

public class HelpMethod {
    public static Employee createEmployee(String first, String last, String user, String pass, Role role) {

        Employee employee = new Employee();
        employee.setFirstName(first);
        employee.setLastName(last);
        employee.setUsername(user);
        employee.setPassword(pass);
        employee.setRole(role);

        return employee;
    }
}
