package cvut.fel.vacation.dao;

import cvut.fel.vacation.VacationApplication;
import cvut.fel.vacation.endpoints.config.TestRestTemplateConfig;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import static cvut.fel.vacation.utils.HelpMethod.createEmployee;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ComponentScan(basePackageClasses = VacationApplication.class, excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = TestRestTemplateConfig.class))
@ActiveProfiles("test")
public class EmployeeDaoTest {
    @Autowired
    private TestEntityManager em;

    @Autowired
    private EmployeeDao sut;

    private final Employee employee = createEmployee("Vincent", "van Gogh", "vanGogh123", "12345", Role.ROLE_ADMIN);

    @Test
    public void findByUsernameReturnsPersonWithMatchingUsername() {
        em.persist(employee);

        final Employee result = sut.findByUsername(employee.getUsername());
        assertNotNull(result);
        assertEquals(employee.getId(), result.getId());
    }


}
