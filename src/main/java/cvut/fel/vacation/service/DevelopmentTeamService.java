package cvut.fel.vacation.service;

import cvut.fel.vacation.dao.DevelopmentTeamDao;
import cvut.fel.vacation.exception.NotValidRequestException;
import cvut.fel.vacation.model.DevelopmentTeam;
import cvut.fel.vacation.model.Employee;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class DevelopmentTeamService extends BaseService<DevelopmentTeam>{

    public DevelopmentTeamService(DevelopmentTeamDao dao) {
        super(dao);
    }

    public void setTeamLeader(DevelopmentTeam team, Employee employee) {
        if (employee.getTeam() != team) {
            throw new NotValidRequestException("Employee is not in the team.");
        }

        team.setTeamLeaderId(employee.getId());
        update(team);
    }
}
