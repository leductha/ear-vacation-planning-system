package cvut.fel.vacation.service;

import cvut.fel.vacation.dao.TeamDao;
import cvut.fel.vacation.dto.TeamDTO;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Team;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class TeamService extends BaseService<Team> {
    private final EmployeeService employeeService;

    public TeamService(TeamDao dao, EmployeeService employeeService) {
        super(dao);
        this.employeeService = employeeService;
    }

    public TeamDTO addEmployeeToTeam(Employee employee, Team team) {

        employee.setTeam(team);
        team.addEmployee(employee);

        employeeService.save(employee);
        save(team);

        TeamDTO dto = TeamDTO.fromEntity(team);
        return dto;
    }

    public void removeTeam(Integer id) {
        Team team = find(id);

        if (team == null) {
            return;
        }

        team.getEmployees().stream()
                .forEach(employee -> employee.setTeam(null));

        remove(team);
    }
}
