package cvut.fel.vacation.service;

import cvut.fel.vacation.dao.EmergencyDao;
import cvut.fel.vacation.dto.EmergencyGetDTO;
import cvut.fel.vacation.exception.NotFoundException;
import cvut.fel.vacation.exception.NotValidRequestException;
import cvut.fel.vacation.model.Emergency;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@Transactional
public class EmergencyService extends BaseService<Emergency>{
    private final EmployeeService employeeService;

    @Autowired
    public EmergencyService(EmergencyDao dao, EmployeeService employeeService) {
        super(dao);
        this.employeeService = employeeService;
    }


    public EmergencyGetDTO createEmergency() {
        Employee employee = getEmployee();

        if (employee == null) {
            throw NotFoundException.create("Employee", employee.getId());
        }

        if (!emergencyValidation(employee)) {
            throw new NotValidRequestException("Emergency is already created");
        }

        Emergency emergency = new Emergency();
        emergency.setDate(LocalDate.now());
        emergency.setEmployee(employee);

        employee.addEmergency(emergency);
        save(emergency);

        return EmergencyGetDTO.fromEntity(emergency);
    }

    public boolean emergencyValidation(Employee employee) {
        AtomicBoolean bool = new AtomicBoolean(true);

        List<Emergency> emergencies = findAll();

        emergencies.stream()
                .filter(emergency -> emergency.getEmployee().getId() == employee.getId())
                .forEach(emergency -> {
                    if (emergency.getDate().equals(LocalDate.now())) {
                        bool.set(false);
                    }
                });

        return bool.get();
    }

    public void removeEmergency(Integer id) {
        Emergency emergency = find(id);

        if (emergency == null) {
            throw new NotValidRequestException("Emergency does not exist");
        }

        if (emergency.getEmployee().getId() != getEmployee().getId()) {
            throw new NotValidRequestException("Not a request created by you.");
        }

        Employee employee = employeeService.find(emergency.getEmployee().getId());
        employee.removeEmergency(emergency);

        remove(emergency);
    }

    public Employee getEmployee() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        Integer userId = ((UserDetailsImpl) userDetails).getId();

        return employeeService.find(userId);
    }
}
