package cvut.fel.vacation.service;

import cvut.fel.vacation.dao.VacationRequestDao;
import cvut.fel.vacation.dto.VacationRequestGetDTO;
import cvut.fel.vacation.dto.VacationRequestPostDTO;
import cvut.fel.vacation.exception.NotFoundException;
import cvut.fel.vacation.exception.NotValidRequestException;
import cvut.fel.vacation.exception.ValidationException;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Role;
import cvut.fel.vacation.model.State;
import cvut.fel.vacation.model.VacationRequest;
import cvut.fel.vacation.security.UserDetailsImpl;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Transactional
public class VacationRequestService extends BaseService<VacationRequest>{
    private final EmployeeService employeeService;

    @Autowired
    public VacationRequestService(VacationRequestDao dao, EmployeeService employeeService) {
        super(dao);
        this.employeeService = employeeService;
    }

    public VacationRequest convertIntoVacationRequest(VacationRequestPostDTO dto, Employee employee) {
        if (employee == null) {
            throw NotFoundException.create("Employee", employee.getId());
        }

        VacationRequest req = new VacationRequest();
        req.setState(State.CREATED);
        req.setToDate(dto.getToDate());
        req.setFromDate(dto.getFromDate());
        req.setEmployee(employee);

        return req;
    }

    @Transactional
    public VacationRequestGetDTO createVacationRequest(VacationRequestPostDTO postDto) {
        Employee employee = getEmployee();

        VacationRequest request = convertIntoVacationRequest(postDto, employee); // set state

        if (!validateVacationRequest(request)) {
            throw NotValidRequestException.create("Vacation dates are not valid.");
        } else if (!employee.isHolidayRequestWithoutOverlaps(request)) {
            throw NotValidRequestException.create("Vacation request overlaps other request.");
        } else if (employee.getNumberOfFreeDays() < request.getNumberOfDays()) {
            throw NotValidRequestException.create("Employee has not enough free days.");
        }

        request.setEmployee(employee);
        employee.addVacationRequest(request);
        employee.subtractNumberOfHolidays(request.getNumberOfDays());

        save(request);

        VacationRequestGetDTO getDto = VacationRequestGetDTO.fromEntity(request);

        return getDto;
    }

    public void removeVacationRequest(Integer id) {
        VacationRequest request = find(id);

        if (request == null) {
            throw new NotValidRequestException("Request does not exist");
        }

        if (request.getEmployee().getId() != getEmployee().getId()) {
            throw new NotValidRequestException("Not a request created by you.");
        }

        Employee employee = employeeService.find(request.getEmployee().getId());
        employee.addNumberOfHolidays(request.getNumberOfDays());
        employee.removeVacationRequest(request);

        remove(request);
    }

    public boolean validateVacationRequest(VacationRequest request) {
        LocalDate fromDate = request.getFromDate();
        LocalDate toDate = request.getToDate();

        if (fromDate.isAfter(toDate)) {
            return false;
        } else if (fromDate.isBefore(LocalDate.now())) {
            return false;
        }

        return true;
    }

    public VacationRequestGetDTO assignAdminToVacationRequest(Integer vacationRequestId) {
        Employee admin = getEmployee();

        VacationRequest request = find(vacationRequestId);
        if (request == null) {
            throw NotFoundException.create("Vacation Request", vacationRequestId);
        }

        request.setAdmin(admin);
        request.setState(State.PENDING);

        save(request);
        VacationRequestGetDTO get = VacationRequestGetDTO.fromEntity(request);

        return get;
    }

    public VacationRequestGetDTO approveVacationRequest(Integer id) {
        Employee admin = getEmployee();

        VacationRequest request = find(id);
        if (request == null) {
            throw NotFoundException.create("Vacation Request", id);
        }

        if (request.getAdmin() != admin) {
            throw new ValidationException("Request is already assigned to someone else.");
        }

        if (request.getState() == State.DENIED) {
            throw new ValidationException("Request is already denied.");
        }
        request.setState(State.APPROVED);

        save(request);
        VacationRequestGetDTO get = VacationRequestGetDTO.fromEntity(request);

        return get;
    }

    public VacationRequestGetDTO denyVacationRequest(Integer id) {
        Employee admin = getEmployee();

        VacationRequest request = find(id);
        if (request == null) {
            throw NotFoundException.create("Vacation Request", id);
        }

        if (request.getAdmin() != admin) {
            throw new ValidationException("Request is already assigned to someone else.");
        }

        if (request.getState() == State.APPROVED) {
            throw new ValidationException("Request is already denied.");
        }

        request.setState(State.DENIED);

        save(request);
        VacationRequestGetDTO get = VacationRequestGetDTO.fromEntity(request);

        return get;
    }

    public Employee getEmployee() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        Integer userId = ((UserDetailsImpl) userDetails).getId();

        return employeeService.find(userId);
    }
}
