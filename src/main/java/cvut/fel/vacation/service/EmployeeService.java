package cvut.fel.vacation.service;

import cvut.fel.vacation.dao.EmergencyDao;
import cvut.fel.vacation.dao.EmployeeDao;
import cvut.fel.vacation.dao.VacationRequestDao;
import cvut.fel.vacation.dto.EmployeePostDTO;
import cvut.fel.vacation.model.Emergency;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Role;
import cvut.fel.vacation.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmployeeService extends BaseService<Employee>{

    private final EmployeeDao employeeDao;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public EmployeeService(EmployeeDao dao, PasswordEncoder passwordEncoder) {
        super(dao);
        this.employeeDao = dao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional(readOnly = true)
    public boolean exists(String username) {
        return ((EmployeeDao) dao).findByUsername(username) != null;
    }

    public Employee convertIntoEmployee(EmployeePostDTO dto) {
        Employee employee = new Employee();
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setUsername(dto.getUsername());
        employee.setPassword(passwordEncoder.encode(dto.getPassword()));
        employee.setRole(Role.values()[dto.getRole()]);
        return employee;
    }

    public Employee createEmployee(EmployeePostDTO dto) {
        Employee employee = convertIntoEmployee(dto);
        save(employee);
        return employee;
    }

    public Team getTeamByEmployeeId (Integer id) {
        Employee employee = dao.find(id);

        if (employee.getTeam() == null) {
            return null;
        }

        return employee.getTeam();
    }

    @Transactional(readOnly = true)
    public Employee findByUsername(String username) {
        return employeeDao.findByUsername(username);
    }

}
