package cvut.fel.vacation.dto;

import cvut.fel.vacation.model.Emergency;
import cvut.fel.vacation.model.Employee;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmergencyGetDTO {
    Integer id;
    LocalDate date;
    Integer employeeId;

    public static EmergencyGetDTO fromEntity(Emergency emergency) {
        EmergencyGetDTO emergencyDTO = new EmergencyGetDTO();
        emergencyDTO.setDate(emergency.getDate());
        emergencyDTO.setId(emergency.getId());
        emergencyDTO.setEmployeeId(emergency.getEmployee().getId());

        return emergencyDTO;
    }
}
