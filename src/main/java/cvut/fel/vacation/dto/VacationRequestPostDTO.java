package cvut.fel.vacation.dto;

import cvut.fel.vacation.model.State;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class VacationRequestPostDTO {
    private LocalDate fromDate;
    private LocalDate toDate;
}
