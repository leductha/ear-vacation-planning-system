package cvut.fel.vacation.dto;

import cvut.fel.vacation.model.Team;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class TeamDTO {
    private Integer id;
    private String name;
    private List<Integer> employeeIds;

    public static TeamDTO fromEntity(Team team){
        TeamDTO dto = new TeamDTO();
        dto.setName(team.getName());
        dto.setId(team.getId());

        if (team.getEmployees() != null) {
            dto.setEmployeeIds(team.getEmployees().stream()
                    .map(employee -> employee.getId())
                    .collect(Collectors.toList()));
        }

        return dto;
    }
}
