package cvut.fel.vacation.dto;

import cvut.fel.vacation.model.Employee;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
@Setter
public class EmployeeGetDTO {
    private Integer id;
    private String firstName;
    private String lastName;
    private String username;
    private String role;
    private List<VacationRequestGetDTO> requestList = new ArrayList<>();
    private List<EmergencyGetDTO> emergencyList = new ArrayList<>();
    private String teamName;

    private Integer freeDays;

    public static EmployeeGetDTO fromEntity(Employee employee) {
        EmployeeGetDTO employeeGetDTO = new EmployeeGetDTO();
        employeeGetDTO.setId(employee.getId());
        employeeGetDTO.setFirstName(employee.getFirstName());
        employeeGetDTO.setLastName(employee.getLastName());
        employeeGetDTO.setUsername(employee.getUsername());
        employeeGetDTO.setRole(employee.getRole().toString());
        employeeGetDTO.setFreeDays(employee.getNumberOfFreeDays());

        if (employee.getRequestList() != null) {
            List<VacationRequestGetDTO> vacationRequestGetDTOS = employee.getRequestList().stream()
                    .map(VacationRequestGetDTO::fromEntity)
                    .collect(Collectors.toList());
            employeeGetDTO.setRequestList(vacationRequestGetDTOS);
        }

        if (employee.getEmergencyList() != null) {
            List<EmergencyGetDTO> dto = employee.getEmergencyList().stream()
                    .map(EmergencyGetDTO::fromEntity)
                    .collect(Collectors.toList());
            employeeGetDTO.setEmergencyList(dto);
        }


        if (employee.getTeam() != null) {
            employeeGetDTO.setTeamName(employee.getTeam().getName());
        }

        return employeeGetDTO;
    }

}
