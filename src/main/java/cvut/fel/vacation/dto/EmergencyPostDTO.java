package cvut.fel.vacation.dto;

import cvut.fel.vacation.model.Emergency;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmergencyPostDTO {
    LocalDate date;

    public static EmergencyPostDTO fromEntity(Emergency emergency) {
        EmergencyPostDTO emergencyDTO = new EmergencyPostDTO();
        emergencyDTO.setDate(emergency.getDate());

        return emergencyDTO;
    }
}
