package cvut.fel.vacation.dto;

import cvut.fel.vacation.model.State;
import cvut.fel.vacation.model.VacationRequest;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class VacationRequestGetDTO {
    private Integer id;
    private LocalDate fromDate;
    private LocalDate toDate;
    private State state;
    private Integer employeeId;
    private Integer adminId;

    public static VacationRequestGetDTO fromEntity(VacationRequest vacationRequest) {
        VacationRequestGetDTO vacationRequestGetDTO = new VacationRequestGetDTO();
        if (vacationRequest.getId() != null) {
            vacationRequestGetDTO.setId(vacationRequest.getId());
        }
        vacationRequestGetDTO.setFromDate(vacationRequest.getFromDate());
        vacationRequestGetDTO.setToDate(vacationRequest.getToDate());
        vacationRequestGetDTO.setState(vacationRequest.getState());


        if (vacationRequest.getEmployee() != null) {
            vacationRequestGetDTO.setEmployeeId(vacationRequest.getEmployee().getId());
        }

        if (vacationRequest.getAdmin() != null) {
            vacationRequestGetDTO.setAdminId(vacationRequest.getAdmin().getId());
        }

        return vacationRequestGetDTO;
    }
}
