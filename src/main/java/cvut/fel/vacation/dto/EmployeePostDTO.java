package cvut.fel.vacation.dto;

import cvut.fel.vacation.model.Employee;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
public class EmployeePostDTO {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private Integer role;
    private String teamName;
}
