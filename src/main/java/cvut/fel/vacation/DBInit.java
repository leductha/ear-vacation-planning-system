package cvut.fel.vacation;

import cvut.fel.vacation.dao.DevelopmentTeamDao;
import cvut.fel.vacation.exception.NotFoundException;
import cvut.fel.vacation.model.DevelopmentTeam;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Role;
import cvut.fel.vacation.model.State;
import cvut.fel.vacation.model.Team;
import cvut.fel.vacation.model.VacationRequest;
import cvut.fel.vacation.service.DevelopmentTeamService;
import cvut.fel.vacation.service.EmployeeService;
import cvut.fel.vacation.service.TeamService;
import cvut.fel.vacation.service.VacationRequestService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


@Service
@Profile("!test")
public class DBInit implements CommandLineRunner {
    @Autowired
    EmployeeService employeeService;

    @Autowired
    VacationRequestService vacationRequestService;

    @Autowired
    TeamService teamService;

    @Autowired
    DevelopmentTeamService devTeamService;

    @Autowired
    PasswordEncoder passwordEncoder;

    private final Random random = new Random();

    @Override
    public void run(String... args) {
        String[] firstNames = {
                "Albert", "Leonardo", "Wolfgang", "Isaac", "Marie",
                "William", "Galileo", "Charles", "Nikola", "Ada",
                "Nelson", "Cleopatra", "Vincent", "Amelia", "Martin",
                "Rosa", "Winston", "Joan", "Mahatma", "Mother"
        };

        String[] lastNames = {
                "Einstein", "da Vinci", "Mozart", "Newton", "Curie",
                "Shakespeare", "Galilei", "Darwin", "Tesla", "Lovelace",
                "Mandela", "Cleopatra", "van Gogh", "Earhart", "Luther",
                "Parks", "Churchill", "of Arc", "Gandhi", "Teresa"
        };

        // CREDENTIALS
        createUser("User", "User", "user", "user");
        createAdmin("Admin", "Admin", "admin", "admin");

        for (int i = 0; i < 10; i++) {
            createUser(firstNames[i], lastNames[i], "user" + i, generateRandomPassword());
        }

        for (int i = 10; i < 20; i++) {
            createAdmin(firstNames[i], lastNames[i], "admin" + i, generateRandomPassword());
        }

        // REQUEST
        for (int i = 0; i < 20; i++) {
            createVacationRequest();
        }

        // TEAM
        String[] teams = { "FaZe", "Fnatic", "Cloud9", "Team Liquid", "Astralis", "Natus Vincere", "G2 Esports", "Virtus.pro", "OG", "ENCE" };
        for (int i = 0; i < 5; i++) {
            createTeam(teams[i]);
        }

        for (int i = 5; i < 10; i++) {
            createDevTeam(teams[i]);
        }



    }

    public void createVacationRequest() {
        LocalDate fromDate = LocalDate.now().plusDays(random.nextInt(100) + 1);

        int randomDays = random.nextInt(21) + 1;
        LocalDate toDate = fromDate.plusDays(randomDays);

        List<Employee> eligibleEmployees = employeeService.findAll().stream()
                .filter(e -> e.getNumberOfFreeDays() >= randomDays)
                .collect(Collectors.toList());

        Employee randomEmployee = eligibleEmployees.get(random.nextInt(eligibleEmployees.size()));

        VacationRequest request = new VacationRequest();
        request.setEmployee(randomEmployee);
        request.setFromDate(fromDate);
        request.setToDate(toDate);
        request.setState(State.CREATED);

        randomEmployee.subtractNumberOfHolidays(request.getNumberOfDays());
        employeeService.update(randomEmployee);

        vacationRequestService.save(request);
    }


    public void createUser(String firstname, String lastname, String username, String password) {
        Employee employee = new Employee();

        employee.setFirstName(firstname);
        employee.setLastName(lastname);
        employee.setUsername(username);
        employee.setPassword(passwordEncoder.encode(password));
        employee.setRole(Role.ROLE_USER);

        employeeService.save(employee);
    }

    public void createAdmin(String firstname, String lastname, String username, String password) {
        Employee employee = new Employee();

        employee.setFirstName(firstname);
        employee.setLastName(lastname);
        employee.setUsername(username);
        employee.setPassword(passwordEncoder.encode(password));
        employee.setRole(Role.ROLE_ADMIN);

        employeeService.save(employee);
    }

    private String generateRandomPassword() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] passwordBytes = new byte[16];
        secureRandom.nextBytes(passwordBytes);
        return Base64.getEncoder().encodeToString(passwordBytes);
    }

    private void createDevTeam(String name) {
        DevelopmentTeam developmentTeam = new DevelopmentTeam();
        developmentTeam.setName(name);
        teamService.save(developmentTeam);
    }

    private void createTeam(String name) {
        Team team = new Team();
        team.setName(name);
        teamService.save(team);
    }

}
