package cvut.fel.vacation.rest;

import cvut.fel.vacation.exception.NotFoundException;
import cvut.fel.vacation.exception.NotValidRequestException;
import cvut.fel.vacation.model.DevelopmentTeam;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.service.DevelopmentTeamService;
import cvut.fel.vacation.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/developmentTeam")
public class DevelopmentTeamController {

    @Autowired
    private final DevelopmentTeamService developmentTeamService;

    @Autowired
    private final EmployeeService employeeService;

    private static final Logger LOG = LoggerFactory.getLogger(DevelopmentTeamController.class);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value="{id}/teamLeader/{employeeId}")
    public void setTeamLeader(@PathVariable Integer id,
                                           @PathVariable Integer employeeId) {
        Employee employee = employeeService.find(employeeId);

        if(employee == null) {
            throw NotFoundException.create("Employee", employeeId);
        }

        DevelopmentTeam team = developmentTeamService.find(id);

        if (team == null) {
            throw NotFoundException.create("DevelopmentTeam", id);
        } else if (team.getClass() != DevelopmentTeam.class) {
            throw new NotValidRequestException("Team is not a Development Team");
        }

        developmentTeamService.setTeamLeader(team, employee);
        LOG.info("Team Leader assigned");
    }

}
