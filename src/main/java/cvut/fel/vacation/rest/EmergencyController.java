package cvut.fel.vacation.rest;

import cvut.fel.vacation.dto.EmergencyGetDTO;
import cvut.fel.vacation.model.Emergency;
import cvut.fel.vacation.service.EmergencyService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/emergency")
public class EmergencyController {
    private static final Logger LOG = LoggerFactory.getLogger(VacationRequestController.class);
    @Autowired
    private final EmergencyService emergencyService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmergencyGetDTO>> getAllEmergency() {
        List<Emergency> emergencies = emergencyService.findAll();

        if (emergencies.isEmpty()) {
            return ResponseEntity.ok(Collections.emptyList());
        }

        List<EmergencyGetDTO> emergencyDTOs = emergencies.stream()
                .map(EmergencyGetDTO::fromEntity)
                .collect(Collectors.toList());

        return ResponseEntity.ok(emergencyDTOs);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmergencyGetDTO> getVacationRequestById(@PathVariable Integer id) {
        try {
            Emergency emergency = emergencyService.find(id);

            if (emergency == null) {
                return ResponseEntity.notFound().build();
            }

            EmergencyGetDTO dto = EmergencyGetDTO.fromEntity(emergency);
            return ResponseEntity.ok(dto);
        } catch (Exception e) {
            return null;
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping
    public ResponseEntity<EmergencyGetDTO> createEmergency() {
        try {
            EmergencyGetDTO emergency = emergencyService.createEmergency();
            LOG.debug("Created request {}.", emergency);
            return ResponseEntity.ok(emergency);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeVacationRequest(@PathVariable Integer id) {
        emergencyService.removeEmergency(id);

        LOG.debug("Removed Emergency {}.", id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
