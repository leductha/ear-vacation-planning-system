package cvut.fel.vacation.rest;

import cvut.fel.vacation.dto.TeamDTO;
import cvut.fel.vacation.dto.VacationRequestGetDTO;
import cvut.fel.vacation.exception.NotFoundException;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Team;
import cvut.fel.vacation.service.EmployeeService;
import cvut.fel.vacation.service.TeamService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/team")
public class TeamController {
    private static final Logger LOG = LoggerFactory.getLogger(TeamController.class);

    @Autowired
    private TeamService teamService;

    @Autowired
    private EmployeeService employeeService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllTeams() {
        List<Team> teams = teamService.findAll();
        List<TeamDTO> dtos = teams.stream()
                .map(TeamDTO::fromEntity)
                .collect(Collectors.toList());

        if (teams.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTeamById(@PathVariable Integer id) {
        Team team = teamService.find(id);

        if (team == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        TeamDTO dto = TeamDTO.fromEntity(team);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createTeam(@RequestBody Team team) {

        teamService.save(team);
        LOG.debug("Team {} was successfully created.", team);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeTeam(@PathVariable Integer id) {
        teamService.removeTeam(id);

        LOG.debug("Removed team with id.", id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value="{id}/addEmployee/{employeeId}")
    public ResponseEntity<?> addEmployeeToTeam(@PathVariable Integer id,
                                               @PathVariable Integer employeeId) {
        Employee employee = employeeService.find(employeeId);

        if(employee == null) {
            throw NotFoundException.create("Employee", employeeId);
        }

        Team team = teamService.find(id);

        if (team == null) {
            throw NotFoundException.create("Team", id);
        }

        TeamDTO get = teamService.addEmployeeToTeam(employee, team);
        return ResponseEntity.ok(get);
    }


}
