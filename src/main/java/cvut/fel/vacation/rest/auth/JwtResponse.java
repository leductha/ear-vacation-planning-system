package cvut.fel.vacation.rest.auth;

import lombok.Data;


@Data
public class JwtResponse {

    private final String token;
    private final String type = "Bearer";
    private final String username;
    private final String authRole;
    private final Integer id;

    public JwtResponse(String accessToken, String username, String authRole, Integer id) {
        this.token = accessToken;
        this.username = username;
        this.authRole = authRole;
        this.id = id;
    }

}