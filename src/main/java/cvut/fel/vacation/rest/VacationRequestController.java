package cvut.fel.vacation.rest;

import cvut.fel.vacation.dto.VacationRequestGetDTO;
import cvut.fel.vacation.dto.VacationRequestPostDTO;
import cvut.fel.vacation.model.VacationRequest;
import cvut.fel.vacation.service.VacationRequestService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/request")
public class VacationRequestController {
    private static final Logger LOG = LoggerFactory.getLogger(VacationRequestController.class);

    @Autowired
    private final VacationRequestService vacationRequestService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VacationRequestGetDTO>> getAllVacationRequest() {
        List<VacationRequest> vacationRequests = vacationRequestService.findAll();

        if (vacationRequests.isEmpty()) {
            return ResponseEntity.ok(Collections.emptyList());
        }

        List<VacationRequestGetDTO> vacationRequestGetDTO = vacationRequests.stream()
                .map(VacationRequestGetDTO::fromEntity)
                .collect(Collectors.toList());

        return ResponseEntity.ok(vacationRequestGetDTO);

    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VacationRequestGetDTO> getVacationRequestById(@PathVariable Integer id) {
        try {
            VacationRequest request = vacationRequestService.find(id);

            if (request == null) {
                return ResponseEntity.notFound().build();
            }

            VacationRequestGetDTO dto = VacationRequestGetDTO.fromEntity(request);
            return ResponseEntity.ok(dto);
        } catch (Exception e) {
            return null;
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping
    public ResponseEntity<VacationRequestGetDTO> createVacationRequest(@RequestBody VacationRequestPostDTO dto) {
        try {
            VacationRequestGetDTO getDTO = vacationRequestService.createVacationRequest(dto);
            LOG.debug("Created request {}.", getDTO);
            return ResponseEntity.ok(getDTO);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeVacationRequest(@PathVariable Integer id) {
        vacationRequestService.removeVacationRequest(id);

        LOG.debug("Removed Vacation Request {}.", id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value="/{id}/assign", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VacationRequestGetDTO> assignManagerToVacationRequest(@PathVariable Integer id) {

        VacationRequestGetDTO dto = vacationRequestService.assignAdminToVacationRequest(id);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value="/{id}/approve", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VacationRequestGetDTO> approveVacationRequest(@PathVariable Integer id) {

        VacationRequestGetDTO dto = vacationRequestService.approveVacationRequest(id);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value="/{id}/deny", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VacationRequestGetDTO> denyVacationRequest(@PathVariable Integer id) {

        VacationRequestGetDTO dto = vacationRequestService.denyVacationRequest(id);
        return ResponseEntity.ok(dto);
    }







}
