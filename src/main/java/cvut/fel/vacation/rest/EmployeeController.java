package cvut.fel.vacation.rest;

import cvut.fel.vacation.dto.EmployeeGetDTO;
import cvut.fel.vacation.dto.EmployeePostDTO;
import cvut.fel.vacation.dto.TeamDTO;
import cvut.fel.vacation.exception.NotFoundException;
import cvut.fel.vacation.model.Employee;
import cvut.fel.vacation.model.Team;
import cvut.fel.vacation.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private final EmployeeService employeeService;

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmployeeGetDTO>> getAllEmployees() {
        List<Employee> employeeList = employeeService.findAll();

        if (employeeList.isEmpty()) {
            return ResponseEntity.ok(Collections.emptyList());
        }

        List<EmployeeGetDTO> employeeGetDTOList = employeeList.stream()
                .map(EmployeeGetDTO::fromEntity)
                .collect(Collectors.toList());

        return ResponseEntity.ok(employeeGetDTOList);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeGetDTO> getById(@PathVariable Integer id) {
        Employee employee = employeeService.find(id);

        if (employee == null) {
            throw NotFoundException.create("Employee", id);
        }

        EmployeeGetDTO employeeGetDTO = EmployeeGetDTO.fromEntity(employee);
        return ResponseEntity.ok(employeeGetDTO);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createEmployee(@RequestBody EmployeePostDTO dto) {
            Employee employee = employeeService.createEmployee(dto);

            LOG.debug("Created employee {}.", employee);
            return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeEmployee(@PathVariable Integer id) {
        Employee employee = employeeService.find(id);

        if (employee == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        employeeService.remove(employee);

        LOG.debug("Removed employee {}.", employee);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}/team", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTeamByEmployeeId(@PathVariable Integer id) {
        Team team = employeeService.getTeamByEmployeeId(id);
        TeamDTO dto = TeamDTO.fromEntity(team);

        if (team == null) {
            return new ResponseEntity<>(team, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/username/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEmployeeByUsername(@PathVariable String username) {
        Employee employee = employeeService.findByUsername(username);
        EmployeeGetDTO dto = EmployeeGetDTO.fromEntity(employee);

        return ResponseEntity.ok(dto);
    }



}
