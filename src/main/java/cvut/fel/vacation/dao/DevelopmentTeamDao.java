package cvut.fel.vacation.dao;

import cvut.fel.vacation.model.DevelopmentTeam;
import org.springframework.stereotype.Repository;

@Repository
public class DevelopmentTeamDao extends BaseDao<DevelopmentTeam>{

    public DevelopmentTeamDao() {
        super(DevelopmentTeam.class);
    }
}
