package cvut.fel.vacation.model;

public enum State {
    APPROVED, DENIED, CREATED, PENDING
}
