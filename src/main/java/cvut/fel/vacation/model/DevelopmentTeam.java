package cvut.fel.vacation.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@DiscriminatorValue("DevelopmentTeam")
@Getter
@Setter
public class DevelopmentTeam extends Team {
    private Integer teamLeaderId;
}
