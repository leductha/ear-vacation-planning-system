package cvut.fel.vacation.model;

import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "employee")
@NamedQueries({
        @NamedQuery(name = "Employee.findByUsername", query = "SELECT e FROM Employee e WHERE e.username = :username"),
})
public class Employee extends AbstractEntity {

    @Basic(optional = false)
    @Column(name="firstname", nullable = false)
    private String firstName;

    @Basic(optional = false)
    @Column(name="lastname", nullable = false)
    private String lastName;

    @Basic(optional = false)
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Basic(optional = false)
    @Column(name="password", nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name="role", nullable = false)
    private Role role;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "team_id")
    private Team team;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("fromDate ASC")
    private List<VacationRequest> requestList;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("date ASC")
    private List<Emergency> emergencyList;

    private Integer numberOfFreeDays = 21;

    public void addVacationRequest(VacationRequest request) {
        Objects.requireNonNull(request);

        if (requestList == null) {
            requestList = new ArrayList<>();
        }

        requestList.add(request);
    }

    public void removeVacationRequest (VacationRequest request) {
        Objects.requireNonNull(request);

        if (requestList == null) {
            return;
        }

        requestList.removeIf(c->Objects.equals(c.getId(), request.getId()));
    }

    public void addEmergency(Emergency emergency) {
        Objects.requireNonNull(emergency);

        if (emergencyList == null) {
            emergencyList = new ArrayList<>();
        }

        emergencyList.add(emergency);
    }

    public void removeEmergency (Emergency emergency) {
        Objects.requireNonNull(emergency);

        if (emergencyList == null) {
            return;
        }

        emergencyList.removeIf(c->Objects.equals(c.getId(), emergency.getId()));
    }

    public boolean isHolidayRequestWithoutOverlaps(VacationRequest request) {

        boolean result = true;

        if (requestList == null) {
            return result;
        }

        for (VacationRequest hr : requestList) {
            if (hr.getFromDate().isEqual(request.getFromDate()) ||
                    hr.getToDate().isEqual(request.getFromDate()) ||
                    hr.getFromDate().isEqual(request.getToDate()) ||
                    hr.getToDate().isEqual(request.getToDate()))  {


                result = false;
                break;
            }

            if (request.getFromDate().isAfter(hr.getFromDate())
                    && request.getFromDate().isBefore(hr.getToDate())) {

                result = false;
                break;
            }

            if (request.getFromDate().isBefore(hr.getFromDate())
                    && request.getToDate().isAfter(hr.getFromDate())) {


                result = false;
                break;
            }
        }
        return result;
    }

    public void subtractNumberOfHolidays(Integer days) {
        if (days > 0) {
            numberOfFreeDays -= days;
        }
    }

    public void addNumberOfHolidays(Integer days) {
        if (days > 0) {
            numberOfFreeDays += days;
        }
    }


}
