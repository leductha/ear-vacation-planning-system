package cvut.fel.vacation.exception;

public class NotValidRequestException extends EarException {

    public NotValidRequestException(String message) {
        super(message);
    }

    public NotValidRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public static NotValidRequestException create(String resourceName) {
        return new NotValidRequestException(resourceName);
    }
}
