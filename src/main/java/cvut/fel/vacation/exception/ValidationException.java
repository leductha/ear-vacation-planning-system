package cvut.fel.vacation.exception;

public class ValidationException extends EarException {
    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public static ValidationException create(String resourceName, Object identifier) {
        return new ValidationException(resourceName + " identified by " + identifier + " was not valid.");
    }
}
