Thang Le Duc, Long Hoang

# Semestrální práce EAR - Vacation Planning System 2023/2024
Aplikace je určená malým týmům k organizaci jejich dovolených a zamezení překrývání volna klíčových členů týmu.

# Celkový popis

**Uživatelé**

    - Zaměstnanci (vývojáři, technická podpora apod.)
        - Plánování dovolené(podání žádosti)
        - Sledování dostupnosti ostatních členů týmu.
        - Přehled o přiřazení pohotovostí
    - Admin
        - Schvalování/zamítnutí žádostí o dovolenou svých podřízených.
        - Dohled o dostupnosti členů týmů
        - Přehled o přiřazení pohotovostí  

**Product Perspective**

Databáze udržuje tyto data:

    - Informace o uživatelech
        - Jméno, Uživatelské jméno, Heslo
    - Žádosti o dovolených(potvrzené, zamítnuté a nevyřízené)
        - Od/do kdy, kdo požádal, kdo vyřídil
    - Pohotovost
        - Kdo jí má na starosti a který den	

**Prostředí systému**

    - Operační systém: Windows, macOS, Linux
    - Databáze: sql database
    - Technologie a jazyk: Java - Spring Boot, Docker
    - Zabezpečení: JWT autentizace

**Funkční požadavky**

    - Zobrazení žádosti o dovolenou od týmu a podle stavu žádosti
    - Zobrazení žádosti o dovolenou od týmu
    - Přidání / odebrání zaměstnance z týmu 
    - Schválení / zamítnutí žádosti o dovolenou 
    - Zobrazení žádosti o dovolenou od uživatele
    - Přidání / úprava / mazání uživatele, týmu
    - Přiřazení role týmového leadera

**NFR**

    - Bezpečnost
        Uživatelksá hesla budou šifrovaná a bude mít opatření oproti neoprávněnému přístupu.

    - Dostupnost
        Střední doba výpadku aplikace bude 1 hodina.

    - Kompabilita
        Aplikace by měla být kompatibilní s různými zařízeními, bez ohledu na operační systém.
	        
# Postup při spouštění aplikace


Přihlašovací údaje pro admina: 

    - username : admin
    - password : admin

Přihlašovací údaje pro usera: 

    - username : user
    - password : user

Admin i user si vygenerují token a použijí ho na endpointech vhodné jejich právům.
# Diagram
![model](src/main/resources/diagram.png)
